<?php

/* @var $this yii\web\View */
/* @var $statement_file app\models\StatementFile */
/* @var $time array */
/* @var $balance array */
/* @var $max_balance float */

use yii\widgets\ActiveForm;

$this->title = 'Gekko test';
?>
<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>
<?= $form->field($statement_file, 'statement')->fileInput() ?>
    <button>Upload</button>
<?php ActiveForm::end() ?>

    <div style="height: 100px;"></div>

<?php
if(count($balance) > 0 && count($time) > 0 && $max_balance > 0) {
    echo \yii2mod\c3\chart\Chart::widget([
        'options' => [
            'id' => 'balance_chart'
        ],
        'clientOptions' => [
            'data' => [
                'x' => 'x',
                'columns' => [
                    $time,
                    $balance
                ],
                'colors' => [
                    'Balance' => '#4EB269',
                ],
            ],
            'axis' => [
                'x' => [
                    'label' => 'Time',
                    'type' => 'category',
                    'tick' => [
                        'count' => 20,
                        //'rotate' => 75,
                        'multiline' => false
                    ],
                    'padding' => ['left' => 0, 'right' => 10]
                ],
                'y' => [
                    'label' => [
                        'text' => 'Balance',
                        'position' => 'outer-top'
                    ],
                    'min' => 0,
                    'max' => $max_balance,
                    'padding' => ['top' => 10, 'bottom' => 0]
                ]
            ]
        ]
    ]);
}
