<?php

namespace app\controllers;

use app\models\ParseStatement;
use app\models\StatementFile;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $statement_file = new StatementFile;
        $balance = [];
        $time = [];
        $max_balance = 0;

        if (Yii::$app->request->isPost) {
            $statement_file->statement = UploadedFile::getInstance($statement_file, 'statement');
            if ($statement_file->upload()) {
                // file is uploaded successfully
                $parsed_data = new ParseStatement($statement_file->file_name);
                $balance = ['Balance'] + $parsed_data->getBalanceArray();
                $time = ['x'] + $parsed_data->getTimeArray();
                $max_balance = $parsed_data->getMaxBalance();
            }
        }

        return $this->render('index', [
            'statement_file' => $statement_file,
            'balance' => $balance,
            'time' => $time,
            'max_balance' => $max_balance,
        ]);
    }
}
