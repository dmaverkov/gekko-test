<?php

namespace app\models;

use yii\base\Model;

/**
 * Statement is the model behind the upload statement form.
 */
class StatementFile extends Model
{
    public $statement;
    public $file_name;

    public function rules()
    {
        return [
            [['statement'], 'file', 'skipOnEmpty' => false, 'extensions' => ['html', 'htm']],
        ];
    }

    public function upload()
    {
        if ($this->validate()) {
            $this->statement->saveAs('uploads/' . $this->statement->baseName . '.' . $this->statement->extension);
            $this->file_name = 'uploads/' . $this->statement->baseName . '.' . $this->statement->extension;
            return true;
        } else {
            return false;
        }
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'statement' => 'Upload statement file',
        ];
    }
}
