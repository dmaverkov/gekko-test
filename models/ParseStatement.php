<?php

namespace app\models;

use simplehtmldom\HtmlDocument;
use yii\base\Model;
use yii\helpers\ArrayHelper;

/**
 * ParseStatement is the model for parse uploaded html file with statement table.
 */
class ParseStatement extends Model
{
    private $body;
    private $rows = [];
    private $profit_history = [];

    //sets default indexes
    private $type_index = 2;
    private $open_time_index = 1;
    private $close_time_index = 8;
    private $profit_index = 13;

    public function __construct($statement_file)
    {
        parent::__construct();

        $this->body = file_get_contents($statement_file);
        $this->parseTable();
    }

    /**
     * parsing html table
     *
     * @return void
     */
    private function parseTable()
    {
        $html = new HtmlDocument();
        $html->load($this->body);
        $table = $html->find('table');
        if ($table != null) {
            $rows = $table[0]->find('tr');
            $row_count = 0;
            $balance = 0;

            foreach ($rows as $row) {
                if ($row->tag  == 'tr') {
                    $tds = [];
                    $td_count = 0;
                    foreach ($row->nodes as $node) {
                        if ($node->tag == 'td') {
                            if (isset($node->attr) && is_array($node->attr) && isset($node->attr['colspan'])) {
                                //correcting column index
                                $td_count += $node->attr['colspan']-1;
                            }
                            $tds[$td_count++] = $node->plaintext;
                        }
                    }

                    //refresh defaults (for anything)
                    if (in_array('Type', $tds) && in_array('Profit', $tds) && in_array('Open Time', $tds)) {
                        $this->type_index = array_search('Type', $tds);
                        $this->profit_index = array_search('Profit', $tds);
                        $this->open_time_index = array_search('Open Time', $tds);
                        if (in_array('Close Time', $tds)) {
                            $this->close_time_index = array_search('Close Time', $tds);
                        }
                    }

                    //save row when buy or sell or balance action
                    if (isset($tds[$this->type_index])) {
                        try {
                            switch ($tds[$this->type_index]) {
                                case "sell":
                                case "buy":
                                case "balance":
                                    $this->rows[$row_count++] = $tds;
                                    $balance += preg_replace("/\s/", "", $tds[$this->profit_index]);
                                    $this->profit_history[] = ['time' => ((isset($tds[$this->close_time_index]) && strlen($tds[$this->close_time_index]) > 0)? $tds[$this->close_time_index] : $tds[$this->open_time_index]), 'balance' => $balance];
                                    break;
                            }
                        } catch (\Exception $e) {
                            //echo $e->getMessage();
                            //echo $tds[$this->profit_index];
                            //die;
                        }
                    }
                }
            }
        }
    }

    public function getRawData()
    {
        return $this->rows;
    }

    public function getProfitHistory()
    {
        return $this->profit_history;
    }

    public function getTimeArray()
    {
        return ArrayHelper::getColumn($this->profit_history,'time');
    }

    public function getBalanceArray()
    {
        return ArrayHelper::getColumn($this->profit_history,'balance');
    }

    public function getMaxBalance()
    {
        if (count($this->profit_history) > 0) {
            return max(ArrayHelper::getColumn($this->profit_history,'balance'));
        } else {
            return 0;
        }
    }
}
